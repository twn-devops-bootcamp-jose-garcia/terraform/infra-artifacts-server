provider "linode" {}

variable "root_pass" {}
variable "authorized_key" {}

resource "linode_instance" "nexus-server" {
  image = "linode/ubuntu22.04"
  label = "terraform-nexus-server"
  region = "us-central"
  type = "g6-standard-4"
  root_pass = var.root_pass
  authorized_keys = [ var.authorized_key ]
}